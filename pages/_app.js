import React from 'react'
import '../styling/site.scss'

export default function MyApp({Component, pageProps}) {
    return <Component {...pageProps} />
}