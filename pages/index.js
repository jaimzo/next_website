import Head from 'next/head'
import React from "react";

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>James O'Brien</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
          <h1>James O'Brien</h1>
      </main>
    </div>
  )
}
