import React from 'react';
import 'react-dom';

const Item = (
    {
        // src, 
        placeholder, 
        title, 
        // isVideo = false, 
        width='auto', 
        height='auto'
    }) => {
    
    return (
        <section className='item'>
            <h2>{title}</h2>
            <div className='srcHolder'>
                <img src={placeholder} width={width} height={height}/>
            </div>
        </section>
    )
}

